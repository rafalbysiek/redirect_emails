<?php
/**
 * {@inheritdoc}
 *
 * PHP Version 7.0
 *
 * @category Email
 * @package  Gatefostware_EmailRedirect
 * @author   GateSoftware <office@gate-software.com>
 * @license  BSD Licence
 * @link     https://www.gate-software.com/pl/sklepy-magento/
 */

/**
 * Class GateSoftware_EmailRedirect_Model_Email_Template
 *
 * @category Email
 * @package  Gatefostware_EmailRedirect
 * @author   GateSoftware <office@gate-software.com>
 * @license  BSD Licence
 * @link     https://www.gate-software.com/pl/sklepy-magento/
 */
class GateSoftware_EmailRedirect_Model_Email_Template
    extends Mage_Core_Model_Email_Template
{

    /**
     * {@inheritdoc}
     * 
     */
    public function send($email, $name = null, array $variables = array())
    {

        if (!$this->isValidForSend()) {
            Mage::logException(new Exception('This letter cannot be sent.')); // translation is intentionally omitted
            return false;
        }

        $emails = array_values((array)$email);
        $names = is_array($name) ? $name : (array)$name;
        $names = array_values($names);
        foreach ($emails as $key => $email) {
            if (!isset($names[$key])) {
                $names[$key] = substr($email, 0, strpos($email, '@'));
            }
        }

        $variables['email'] = reset($emails);
        $variables['name'] = reset($names);

        $this->setUseAbsoluteLinks(true);
        $text = $this->getProcessedTemplate($variables, true);
        $subject = $this->getProcessedTemplateSubject($variables);

        $setReturnPath = Mage::getStoreConfig(
            self::XML_PATH_SENDING_SET_RETURN_PATH
        );
        switch ($setReturnPath) {
        case 1:
                $returnPathEmail = $this->getSenderEmail();
            break;
        case 2:
                $returnPathEmail = Mage::getStoreConfig(
                    self::XML_PATH_SENDING_RETURN_PATH_EMAIL
                );
            break;
        default:
                $returnPathEmail = null;
            break;
        }

        if ($this->hasQueue() && $this->getQueue() instanceof Mage_Core_Model_Email_Queue) {
            /**
             * Magento email queue
             *
             * @var $emailQueue Mage_Core_Model_Email_Queue
             */
            $emailQueue = $this->getQueue();
            $emailQueue->clearRecipients();
            $emailQueue->setMessageBody($text);
            $emailQueue->setMessageParameters(
                array(
                    'subject'           => $subject,
                    'return_path_email' => $returnPathEmail,
                    'is_plain'          => $this->isPlain(),
                    'from_email'        => $this->getSenderEmail(),
                    'from_name'         => $this->getSenderName(),
                    'reply_to'          => $this->getMail()->getReplyTo(),
                    'return_to'         => $this->getMail()->getReturnPath(),
                )
            )
                ->addRecipients(
                    $emails,
                    $names,
                    Mage_Core_Model_Email_Queue::EMAIL_TYPE_TO
                )
                ->addRecipients(
                    $this->_bccEmails,
                    array(),
                    Mage_Core_Model_Email_Queue::EMAIL_TYPE_BCC
                );
            $emailQueue->addMessageToQueue();

            return true;
        }

        ini_set('SMTP', Mage::getStoreConfig('system/smtp/host'));
        ini_set('smtp_port', Mage::getStoreConfig('system/smtp/port'));

        $mail = $this->getMail();

        if ($returnPathEmail !== null) {
            $mailTransport = new Zend_Mail_Transport_Sendmail("-f".$returnPathEmail);
            Zend_Mail::setDefaultTransport($mailTransport);
        }

        foreach ($emails as $key => $email) {
            $mail->addTo($email, '=?utf-8?B?' . base64_encode($names[$key]) . '?=');
        }

        if ($this->isPlain()) {
            $mail->setBodyText($text);
        } else {
            $mail->setBodyHTML($text);
        }

        //Email redirection
        $email_headers = $mail->getHeaders();
        $store_id =  Mage::app()->getStore()->getId();
        $redirect_enable = Mage::getStoreConfig(
            'emailredirect_options/email_redirect_form/enable_redirect_emails',
            $store_id
        );
        $bcc_redirect_enable = Mage::getStoreConfig(
            'emailredirect_options/bcc_redirect_form/enable_redirect_bcc_emails',
            $store_id
        );
        $cc_redirect_enable = Mage::getStoreConfig(
            'emailredirect_options/cc_redirect_form/enable_redirect_cc_emails',
            $store_id
        );
        if ($redirect_enable) {

            $newemail = Mage::getStoreConfig(
                'emailredirect_options/email_redirect_form/test_email_address',
                $store_id
            );

            if (!is_null($newemail) && !is_null($email_headers['To'])) {
                $mail->clearRecipients();
                $mail->addTo($newemail);

                if ($bcc_redirect_enable && !is_null($email_headers['Bcc'])) {
                    $redirected_bcc = Mage::getStoreConfig(
                        'emailredirect_options/bcc_redirect_form/test_bcc_email_address',
                        $store_id
                    );
                    $mail->addBcc($redirected_bcc);
                }
                if ($cc_redirect_enable && !is_null($email_headers['Cc'])) {
                    $redirected_cc = Mage::getStoreConfig(
                        'emailredirect_options/cc_redirect_form/test_cc_email_address',
                        $store_id
                    );
                    $mail->addCc($redirected_cc);
                }
            }

        }

        //end email redirection
        $mail->setSubject('=?utf-8?B?' . base64_encode($subject) . '?=');
        $mail->setFrom($this->getSenderEmail(), $this->getSenderName());


        try {
            $mail->send();
            $this->_mail = null;
        } catch (Exception $e) {
            $this->_mail = null;
            Mage::logException($e);
            return false;
        }

        return true;
    }

}
