<?php

/**
 * Data class file
 *
 * PHP Version 7.0
 *
 * @category Email
 * @package  Gatefostware_EmailRedirect
 * @author   GateSoftware <office@gate-software.com>
 * @license  BSD Licence
 * @link     https://www.gate-software.com/pl/sklepy-magento/
 */

/**
 * Class GateSoftware_EmailRedirect_Helper_Data
 *
 * @category Email
 * @package  Gatefostware_EmailRedirect
 * @author   GateSoftware <office@gate-software.com>
 * @license  BSD Licence
 * @link     https://www.gate-software.com/pl/sklepy-magento/
 */
class GateSoftware_EmailRedirect_Helper_Data extends Mage_Core_Helper_Abstract
{
}